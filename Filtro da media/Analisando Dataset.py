import pandas as pd
import numpy as np
import matplotlib.pyplot as plt # Plotagem de graficos



# Cabeçalhos
cabecalho = ["INCIDENT_NUMBER","OFFENSE_CODE","OFFENSE_CODE_GROUP","OFFENSE_DESCRIPTION","DISTRICT"	,"REPORTING_AREA","SHOOTING","OCCURRED_ON_DATE","YEAR","MONTH","DAY_OF_WEEK","HOUR","UCR_PART","STREET","Lat","Long","Location"]



# Lendo arquivo CSV
df = pd.read_csv("files/crimeLESS.csv",encoding="ISO-8859-1",header=None, names=cabecalho,index_col=0)

#Acesso aos index
#print(df["YEAR"].value_counts())


#print(df["OFFENSE_CODE_GROUP"].value_counts())

#df["OFFENSE_CODE_GROUP"].value_counts().plot()


