import numpy
import Image
import time

# Função de media simples
def median_filter(data, filter_size):
    temp = []
    indexer = filter_size // 2
    data_final = []
    data_final = numpy.zeros((len(data),len(data[0])))
    # Tamanho da imagem
    for i in range(len(data)):
        #
        for j in range(len(data[0])):
            # Tamanho do filtro utilizado
            for z in range(filter_size):
                if i + z - indexer < 0 or i + z - indexer > len(data) - 1:
                    for c in range(filter_size):
                        temp.append(0)
                else:
                    if j + z - indexer < 0 or j + indexer > len(data[0]) - 1:
                        temp.append(0)
                    else:
                        for k in range(filter_size):
                            temp.append(data[i + z - indexer][j + k - indexer])

            #
            temp.sort()
            data_final[i][j] = temp[len(temp) // 2]
            temp = []
    return data_final


def main():
    # Abre a imagem
    img = Image.open("noisyimg.png").convert("L")
    #
    arr = numpy.array(img)
    # A função median_filter recebe 2 paramentros, sendo o segundo o tamnho da área do filtro
    removed_noise = median_filter(arr, 3) 
    img = Image.fromarray(removed_noise)
    # Abre a imagem na tela
    img.show()

    # Para salvar a imagem tem que converter
  
    img = img.convert("L")
    img.save('imagemComFiltro.png', format='PNG')


#main()
#time.sleep(1000)