import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import time # Lib de tempo
import codecs # UTF-8


# https://medium.com/data-hackers/uma-introdu%C3%A7%C3%A3o-simples-ao-pandas-1e15eea37fa1
#notas = pd.Series([2,7,5,10,6], index=["Wilfred", "Abbie", "Harry", "Julia",
#"Carrie"])


#print(notas)

#print("Média:", notas.mean())
#print(notas.describe())
#print("-------")
#print(notas**2)




#df = pd.DataFrame({'Aluno' : ["Wilfred", "Abbie", "Harry", "Julia", "Carrie"],
 #                  'Faltas' : [3,4,2,1,4],
 #                  'Prova' : [2,7,5,10,6],
 #                  'Seminário': [8.5,7.5,9.0,7.5,8.0]})
#print(df.describe())


#doc = codecs.open("files/crime.csv","rU","UTF-16") #open for reading with
#"universal" type set

#df = pd.read_csv(doc, sep=',')


# Cabeçalho
cabecalho = ["INCIDENT_NUMBER","OFFENSE_CODE","OFFENSE_CODE_GROUP","OFFENSE_DESCRIPTION","DISTRICT"	,"REPORTING_AREA","SHOOTING","OCCURRED_ON_DATE","YEAR","MONTH","DAY_OF_WEEK","HOUR","UCR_PART","STREET","Lat","Long","Location"]



# Funcionando
#df = pd.read_csv("files/crimeLESS.csv", sep="\t",
#skiprows=5,encoding="ISO-8859-1",engine="python")

# Teste 2
df = pd.read_csv("files/crimeLESS.csv",encoding="ISO-8859-1",header=None, names=cabecalho,index_col=0)

# Não funciona
#print(df["OFFENSE_CODE_GROUP"].value_counts())
#print(df["OFFENSE_CODE_GROUP"].value_counts())
print(df["YEAR"].value_counts())


#print(df)
#print(df.loc[3])